import React, { useEffect } from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";
import * as Permissions from "expo-permissions"; //ask locations permissions
import * as TaskManager from "expo-task-manager"; //task manager for background task
import * as Location from "expo-location"; //location

const LOCATION_TASK_NAME = "background-location-task";

TaskManager.defineTask("location_update", ({ data: { locations }, error }) => {
  if (error) {
    // check `error.message` for more details.
    return;
  }
  console.log("Received new locations", locations);
});

export default function App() {
  //componentDidMount equivalent
  useEffect(() => {
    geoLocationPermission();
  }, []);

  //asking for location permission
  const geoLocationPermission = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== "granted") {
      console.log("Permission denied");
    } else {
      startGeoFencing();
    }
  };

  const onPress = async () => {
    Location.getCurrentPositionAsync({ accuracy: Location.Accuracy.High }).then(
      location => console.log(location)
    );
  };

  //start geofencing task
  const startGeoFencing = () => {
    const regions = [
      { latitude: -7.2327378, longitude: 112.7685437, radius: 100 }
    ];

    Location.enableNetworkProviderAsync().then(() => console.log("success"));
    Location.startGeofencingAsync("geofencing_task", regions).then(() =>
      console.log("done")
    );

    // Location.startLocationUpdatesAsync("location_update", {
    //   accuracy: Location.Accuracy.Balanced
    // });

    TaskManager.getRegisteredTasksAsync().then(data => console.log(data));

    Location.hasStartedGeofencingAsync("geofencing_task").then(data =>
      console.log(data)
    );
  };

  return (
    <TouchableOpacity
      style={{
        justifyContent: "center",
        alignContent: "center",
        alignSelf: "center",
        alignItems: "center",
        height: "100%"
      }}
      onPress={onPress}
    >
      <Text>Enable background location</Text>
    </TouchableOpacity>
  );
}

//defining background task
TaskManager.defineTask(
  "geofencing_task",
  ({ data: { eventType, region }, error }) => {
    if (error) {
      // check `error.message` for more details.
      console.log(error.message);
      return;
    }
    console.log(region);
    if (eventType === Location.GeofencingEventType.Enter) {
      console.log("You've entered region:", region);
    } else if (eventType === Location.GeofencingEventType.Exit) {
      console.log("You've left region:", region);
    }
  }
);

TaskManager.defineTask(LOCATION_TASK_NAME, ({ data, error }) => {
  if (error) {
    // Error occurred - check `error.message` for more details.
    console.log(error);
    return;
  }
  if (data) {
    const { locations } = data;
    console.log(locations);
    // do something with the locations captured in the background
  }
});
